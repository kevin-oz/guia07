/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.prn335_2018.flota.guia07.control;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import org.mockito.runners.MockitoJUnitRunner;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.TipoEstadoReserva;

/**
 *
 * @author kevin Figueroa
 */
@RunWith(MockitoJUnitRunner.class)
public class UtilidadesTest {

    @Mock
    EntityManager ema;
    EntityTransaction transaction;

    TypedQuery<TipoEstadoReserva> t;
    javax.persistence.Query query;

    public UtilidadesTest() {
    }

    @Before
    public void setUp() {

        List<TipoEstadoReserva> ls = crearListaRegistros();
        List<TipoEstadoReserva> registry = unRegistroLista();
     

        transaction = Mockito.mock(EntityTransaction.class);
        query = Mockito.mock(javax.persistence.Query.class);
        t = Mockito.mock(TypedQuery.class);

        Mockito.when(query.getSingleResult()).thenReturn(new Long(2));
        Mockito.when(ema.createQuery("SELECT COUNT(ter.idTipoEstadoReserva) FROM TipoEstadoReserva ter")).thenReturn(query);

        Mockito.when(t.getResultList()).thenReturn(ls);
        Mockito.when(ema.createQuery("SELECT ter FROM TipoEstadoReserva ter", TipoEstadoReserva.class)).thenReturn(t);

        Mockito.when(query.getResultList()).thenReturn(registry);
        Mockito.when(ema.createQuery("SELECT t FROM TipoEstadoReserva t WHERE t.idTipoEstadoReserva = :idTipoEstadoReserva")).thenReturn(query);

        Mockito.when(t.getResultList()).thenReturn(ls);
        Mockito.when(ema.createQuery("SELECT tpe FROM TipoEstadoReserva tpe WHERE tpe.idTipoEstadoReserva>=:ini", TipoEstadoReserva.class)).thenReturn(t);

        Mockito.when(ema.getTransaction()).thenReturn(transaction);

    }

    /**
     * Test of Insert method, of class Utilidades.
     */
    @Test
    public void testInsert() {
        System.out.println("Insert");
        TipoEstadoReserva ter = crearRegistro(1, "En proceso", true, true, "lalalala");

        Utilidades util = new Utilidades();
        util.em = ema;
        doNothing().when(ema).persist(ter);
        util.insert(ter);
        verify(ema).persist(ter);
        //fail("¿fallo?, ¿donde? :v");

    }

    /**
     * Test of Update method, of class Utilidades.
     */
    @Test
    public void testUpdate() {
        System.out.println("Update");
        TipoEstadoReserva ter = crearRegistro(1, "En proceso", true, true, "lalalala");

        Utilidades util = new Utilidades();
        util.em = ema;
        util.update(ter);
        verify(ema).merge(ter);
        //fail("¿fallo?, ¿donde? :v");

    }

    /**
     * Test of Delete method, of class Utilidades.
     */
    @Test
    public void testDelete() {
        System.out.println("Delete");
        TipoEstadoReserva ter = crearRegistro(1, "En proceso", true, true, "lalalala");

        doNothing().when(ema).remove(ter);
        Utilidades util = new Utilidades();
        util.em = ema;
        util.delete(ter);
        verify(ema).remove(ema.merge(ter));

        //fail("¿fallo?, ¿donde? :v");
    }

    /**
     * Test of selectAll method, of class Utilidades.
     */
    @Test
    public void testSelectAll() throws Exception {
        System.out.println("selectAll");

        List<TipoEstadoReserva> ter = crearListaRegistros();
        Utilidades util = new Utilidades();
        util.em = ema;
        List<TipoEstadoReserva> actual = util.selectAll();

        assertEquals(ter, actual);

        //fail("¿fallo?, ¿donde? :v");
    }

    /**
     * Test of countAll method, of class Utilidades.
     */
    @Test
    public void testCountAll() throws Exception {
        System.out.println("countAll");
        Utilidades util = new Utilidades();
        util.em = ema;
        int expResult = 2;
        int result = util.countAll();
        assertEquals(expResult, result);

        //fail("¿fallo?, ¿donde? :v");
    }

    /**
     * Test of findByID method, of class Utilidades.
     */
    @Test
    public void testFindByID() throws Exception {
        System.out.println("findByID");

        List<TipoEstadoReserva> registry = unRegistroLista();
        Utilidades util = new Utilidades();
        util.em = ema;
        List<TipoEstadoReserva> actual = util.findByID(registry.get(0).getIdTipoEstadoReserva());
        assertEquals(registry, actual);

        //fail("¿fallo?, ¿donde? :v");
    }

    /**
     * Test of findRange method, of class Utilidades.
     */
    @Test
    public void testfindRange() {
        System.out.println("findRange");
        List<TipoEstadoReserva> ter = crearListaRegistros();

        Utilidades util = new Utilidades();
        util.em = ema;
        List<TipoEstadoReserva> actual = util.findRange(1, 3);

        assertEquals(ter, actual);

        // fail("¿fallo?, ¿donde? :v");
    }

     /**
     * Test of findByMultiple method, of class Utilidades.
     */
    @Test
    public void testfindByMultiple(){
        
        Utilidades util = new Utilidades();
        util.em=ema;
         String[] campos = {"idTipoEstadoReserva","nombre","activo","indicaAprobacion","observaciones"};
            String[] busquedas = {"1","En Proceso","true", "false","Ninguna"}; 
        
        String q = "SELECT tp FROM TipoEstadoReserva tp WHERE tp." + campos[0] + "= '" + busquedas[0]+"'";
        if (campos.length > 1) {
            for (int i = 1; i < campos.length; i++) {
                q += " and tp." + campos[i] + " =  '"+busquedas[i]+"'";
            }
        }
        Mockito.when(t.getResultList()).thenReturn(unRegistroLista());
        Mockito.when(ema.createQuery(q, TipoEstadoReserva.class)).thenReturn(t);
        List<TipoEstadoReserva> ter = util.findByMultiple(campos, busquedas);
        
        assertEquals(ter, unRegistroLista());
        
         // fail("¿fallo?, ¿donde? :v"); 
    }
    
    /**
     * 
     * @param id identificador de la entidad
     * @param name nombre de la entidad a crear
     * @param activo estado de la entidad
     * @param aprob estado del registro
     * @param observacion alguna descripcion 
     * @return 
     */
    public static TipoEstadoReserva crearRegistro(int id, String name, boolean activo, boolean aprob, String observacion) {

        TipoEstadoReserva ter = new TipoEstadoReserva();
        ter.setIdTipoEstadoReserva(id);
        ter.setNombre(name);
        ter.setActivo(activo);
        ter.setIndicaAprobacion(aprob);
        ter.setObservaciones(observacion);

        return ter;

    }

    /**
     * 
     * @return lista con varios registros 
     */
    public static List<TipoEstadoReserva> crearListaRegistros() {
        List<TipoEstadoReserva> listirijilla = new ArrayList<>();
        listirijilla.add(crearRegistro(1, "Aprobada", true, true, "parangaricutirimicuaro"));
        listirijilla.add(crearRegistro(2, "Denegada", true, true, "gucci gang gucci gang"));
        listirijilla.add(crearRegistro(3, "En Proceso", true, true, "lalalala"));

        return listirijilla;
    }
/**
 * 
 * @return lista con un solo registro
 */
    public static List<TipoEstadoReserva> unRegistroLista() {
        List<TipoEstadoReserva> listirijilla = new ArrayList<>();
        listirijilla.add(crearRegistro(1,"En Proceso", true,false, "Ninguna"));

        return listirijilla;
    }

}
